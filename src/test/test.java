package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.SSLOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TableMetadata;

public class test {

	private Cluster _Cluster;
	private Session session;
	private String truststorefilelocation = "C:\\Pentaho-dev\\truststore";
	private File _truststoreFile;
	private String _truststorePassword = "changeit";
	private boolean _sslEnabled=true;
	private String _cassUsername = "";
	private String _cassPassword = "";
	private int 							_defaultPort = 9042;
	private String _hosts = "10.129.4.7:9042,10.129.5.209:9042,10.129.6.41:9042";
	Collection<InetSocketAddress> _Addresses;

	public static void main(String args[]) {
		new test();
	}

	public test() {
		_truststoreFile = new File(truststorefilelocation);
		
		this._Addresses = new LinkedList<InetSocketAddress>(); 
        if (_hosts != null && _hosts.trim().length() > 0) { 
            for (final String pair : _hosts.split(",")) { 
                final String[] parts = pair.split(":"); 
                if (parts.length > 0) { 
                    final String host = parts[0].trim(); 
                    final int port = parts.length > 1 ? Integer.parseInt(parts[1]) : _defaultPort; 
                    this._Addresses.add(new InetSocketAddress(host, port)); 
                } 
            } 
        } 
        else {
        	System.out.println("No host(s) for cassandra provided");
        }

		if (_sslEnabled == true) {
			if (_cassUsername.trim().length() + _cassPassword.trim().length() > 0) {
				System.out.println("using SSL with user and pass");
				// SSL, with user+ pass
				_Cluster = Cluster.builder()
						.addContactPointsWithPorts(_Addresses)
						.withCredentials(_cassUsername, _cassPassword)
						.withSSL(getSSLOptions()).build();
				
				System.out.println(_Cluster.getClusterName());

			} else {
				System.out.println("using SSL without user and pass");
				// SSL, no user+ pass
				_Cluster = Cluster.builder()
						.addContactPointsWithPorts(_Addresses)
						.withSSL(getSSLOptions()).build();
				
				System.out.println(_Cluster.getClusterName());
			}
		} else {
			if (_cassUsername.trim().length() + _cassPassword.trim().length() > 0) {
				System.out.println("No SSL with user and pass");
				// No SSL, with user+pass
				_Cluster = Cluster.builder()
						.addContactPointsWithPorts(_Addresses)
						.withCredentials(_cassUsername, _cassPassword).build();
			} else {
				System.out.println("No SSL without user and pass");
				// No SSL, no user+pass
				_Cluster = Cluster.builder()
						.addContactPointsWithPorts(_Addresses).build();
			}
		}

		_Cluster = Cluster.builder().addContactPoint("10.129.0.127")
				.withPort(Integer.parseInt("9042")).build();

		session = _Cluster.connect("aegon");
		session.close();
		// System.out.println(session.getLoggedKeyspace());
		List<ColumnMetadata> columns = _Cluster.getMetadata().getKeyspace("aegon").getTable("bram_test").getColumns();
		Iterator<ColumnMetadata> columniter = columns.iterator();
		while (columniter.hasNext()) {
			ColumnMetadata column = columniter.next();
			System.out.println("Columnname: " + column.getName() + " datatype: " + column.getType() + " datatype try 2 " + _Cluster.getMetadata().getKeyspace("aegon").getTable("bram_test").getColumn(column.getName()).getType().getName().name());
			System.out.println("+++++++++++++++++"+_Cluster.getMetadata().getKeyspace("aegon").getTable("bram_test").getPrimaryKey().toString());
		}
		
		Collection<TableMetadata> list = _Cluster.getMetadata()
				.getKeyspace("aegon").getTables();

		Iterator<TableMetadata> iter = list.iterator();
		while (iter.hasNext()) {
			TableMetadata row = iter.next();

			System.out.println(row.getName());
		}
		System.out.println(_Cluster.getMetadata().getClusterName());
		// session.close();

		System.exit(1);

	}
	
	private SSLOptions getSSLOptions( ) {

		//Optional<String> keystorePath = getInputNativeSSLKeystorePath(conf);
		//Optional<String> keystorePassword = getInputNativeSSLKeystorePassword(conf);
		//Optional<String> cipherSuites = getInputNativeSSLCipherSuites(conf);

		if ( _truststoreFile.canRead() && _truststorePassword.trim().length() > 0 ) {
			SSLContext context;
			try {
				context = getSSLContext();
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
			String[] css = SSLOptions.DEFAULT_SSL_CIPHER_SUITES;

			return new SSLOptions(context,css);
		}
		return null;
	}
	
	private SSLContext getSSLContext()
			throws NoSuchAlgorithmException, KeyStoreException, CertificateException, 
			IOException, UnrecoverableKeyException, KeyManagementException  {

		FileInputStream tsf = null;
		//FileInputStream ksf = null;
		SSLContext ctx = null;
		try  {
			
			ctx = SSLContext.getInstance("SSL");
			
			tsf = new FileInputStream(_truststoreFile);
			KeyStore ts = KeyStore.getInstance("JKS");
			ts.load(tsf, _truststorePassword.toCharArray());
			TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			tmf.init(ts);

			/*KeyStore ks = KeyStore.getInstance("JKS");
	            ks.load(ksf, keystorePassword.toCharArray());
	            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
	            kmf.init(ks, keystorePassword.toCharArray());*/

			ctx.init(null, tmf.getTrustManagers(), new SecureRandom());
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally {
			tsf.close();
		}
		return ctx;
	}

}
