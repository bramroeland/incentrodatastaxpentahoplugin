package com.incentro.cassandra.datastax;

import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableMap;

public class Utils {
	public static final String COMPRESSION_NONE = "None";
	public static final String COMPRESSION_SNAPPY = "Snappy";
	public static final String COMPRESSION_LZ4 = "LZ4";

	private static final LoadingCache<Map<String, String>, CassandraConnection> cache = CacheBuilder.newBuilder()
			.build(new CacheLoader<Map<String, String>, CassandraConnection>() {
				@Override
				public CassandraConnection load(Map<String, String> config) throws Exception {
					return new CassandraConnection(config, cache);
				}
			});

	public static CassandraConnection connect(final String nodes, final String port, final String username, final String password,
			final String keyspace, final boolean withSSL, final String truststoreFilePath, final String truststorePass,
			final String compression) throws CassandraConnectionException {

		Map<String, String> config = buildConfig(nodes, port, username, password, keyspace, withSSL, truststoreFilePath, truststorePass,
				compression);
		try {
			while (true) {
				// Get (or create) the CassandraConnection from the cache
				CassandraConnection connection = cache.get(config);

				if (connection.acquire()) return connection;

				// If we failed to acquire, it means we raced with the release of the last reference to the connection
				// (which also removed it from the cache).
				// Loop to try again, which will cause the cache to create a new instance.
			}
		}
		catch (ExecutionException e) {
			throw new CassandraConnectionException("Failed to get a connection", e);
		}
	}

	// We use this as a key in our cache: two clients asking for the same configuration at the same time will get back the same object
	private static Map<String, String> buildConfig(final String nodes, final String port, final String username, final String password,
			final String keyspace, final boolean withSSL, final String truststoreFilePath, final String truststorePass,
			final String compression) {
		ImmutableMap.Builder<String, String> builder = ImmutableMap.<String, String> builder().put("nodes", nodes).put("port", port)
				.put("withSSL", Boolean.toString(withSSL));
		if (username != null) builder.put("username", username);
		if (password != null) builder.put("password", password);
		if (keyspace != null) builder.put("keyspace", keyspace);
		if (truststoreFilePath != null) builder.put("truststoreFilePath", truststoreFilePath);
		if (truststorePass != null) builder.put("truststorePass", truststorePass);
		if (compression != null) builder.put("compression", compression);
		return builder.build();
	}
}
