package com.incentro.cassandra.datastax;

public class CassandraConnectionException extends Exception {
	private static final long serialVersionUID = -6591356471259909167L;

	public CassandraConnectionException() {
		super();
	}

    public CassandraConnectionException(String message) {
        super(message);
    }

    public CassandraConnectionException(String message, Throwable cause) {
        super(message, cause);
    }
}
