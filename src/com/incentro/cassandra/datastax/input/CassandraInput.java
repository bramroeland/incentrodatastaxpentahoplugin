package com.incentro.cassandra.datastax.input;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.row.RowMeta;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStep;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.incentro.cassandra.datastax.CassandraConnection;
import com.incentro.cassandra.datastax.CassandraConnectionException;
import com.incentro.cassandra.datastax.Utils;

/**
 * Class providing an input step for reading data from a Cassandra table (column family).
 * 
 * @author Rudmer van Dijk (rudmer{[dot]}vandijk{[at]}incentro{[dot]}com)
 */
public class CassandraInput extends BaseStep implements StepInterface {

	private static Class<?> PKG = CassandraInput.class;

	private CassandraInputMeta meta;
	private CassandraInputData data;

	private CassandraConnection connection;

	private String[] rowColumns;

	private String nodes;

	private String port;

	private String username;

	private String password;

	private String keyspace;

	private Boolean withSSL;

	private String trustStoreFilePath;

	private String trustStorePass;

	private Boolean executeForEachInputRow = false;

	private String cqlStatement;

	public CassandraInput(StepMeta stepMeta, StepDataInterface stepDataInterface, int copyNr, TransMeta transMeta,
			Trans trans) {
		super(stepMeta, stepDataInterface, copyNr, transMeta, trans);
	}

	public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {
		meta = (CassandraInputMeta) smi;
		data = (CassandraInputData) sdi;
		ResultSet rs = null;

		Object[] r = getRow(); // Get row from input rowset & set row busy!
		if (executeForEachInputRow && r == null) {
			setOutputDone(); // signal end to receiver(s)
			return false;
		}

		if (first) {
			first = false;

			data.outputRowMeta = new RowMeta();
			rs = connection.getSession().execute(cqlStatement);
			meta.createOutputRowMeta(data.outputRowMeta, rs);

			rowColumns = data.outputRowMeta.getFieldNames();
		}

		try {
			while (!rs.isExhausted()) {
				Object[] outputRow = new Object[rowColumns.length];
				Row row = rs.one();
				incrementLinesInput();

				int i = 0;
				for (String col : rowColumns) {
					Object o = row.getObject(col);
					if (o instanceof Integer) {
						o = new Long((Integer)o);
					}
					outputRow[i++] = o;
				}
				putRow(data.outputRowMeta, outputRow); // copy row to output rowset(s);
			}

		} catch (KettleException e) {
			logError(BaseMessages.getString(PKG, "DimensionLookup.Log.StepCanNotContinueForErrors", e.getMessage()));
			logError(Const.getStackTracker(e));
			setErrors(1);
			stopAll();
			setOutputDone(); // signal end to receiver(s)
			return false;
		}

		setOutputDone(); // signal end to receiver(s)
		return false;

	}

	public boolean init(StepMetaInterface smi, StepDataInterface sdi) {
		// Casting to step-specific implementation classes is safe
		meta = (CassandraInputMeta) smi;
		data = (CassandraInputData) sdi;
		nodes = environmentSubstitute(meta.getCassadraNodes());
		port = environmentSubstitute(meta.getCassandraPort());
		username = environmentSubstitute(meta.getUsername());
		password = environmentSubstitute(meta.getPassword());
		keyspace = environmentSubstitute(meta.getKeyspace());
		withSSL = meta.getWithSSL();
		trustStoreFilePath = environmentSubstitute(meta.getTrustStoreFilePath());
		trustStorePass = environmentSubstitute(meta.getTrustStorePass());
		executeForEachInputRow = meta.getExecuteForEachInputRow();
		cqlStatement = environmentSubstitute(meta.getCqlStatement());

		try {
			this.connection = Utils.connect(nodes, port, username, password, keyspace, withSSL,
					trustStoreFilePath, trustStorePass, Utils.COMPRESSION_SNAPPY);
		} catch (CassandraConnectionException e) {
			logError("Could initialize step: " + e.getMessage());
		}

		return super.init(meta, data);
	}

	public void dispose(StepMetaInterface smi, StepDataInterface sdi) {
		if (connection != null)
			connection.release();
		super.dispose(meta, data);
	}

	public Session getSession() {
		return this.connection.getSession();
	}
}