package com.incentro.cassandra.datastax.input;

import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.step.BaseStepData;
import org.pentaho.di.trans.step.StepDataInterface;

public class CassandraInputData extends BaseStepData implements StepDataInterface {

	/** The input data format */
	protected RowMetaInterface inputRowMeta;
	/** The output data format */
	protected RowMetaInterface outputRowMeta;

	/**
	 * Get the input row format
	 * 
	 * @return the input row format
	 */
	public RowMetaInterface getInputRowMeta() {
		return inputRowMeta;
	}

	/**
	 * Set the input row format
	 * 
	 * @param rmi
	 *            the input row format
	 */
	public void setInputRowMeta(RowMetaInterface rmi) {
		inputRowMeta = rmi;
	}

	/**
	 * Get the output row format
	 * 
	 * @return the output row format
	 */
	public RowMetaInterface getOutputRowMeta() {
		return outputRowMeta;
	}

	/**
	 * Set the output row format
	 * 
	 * @param rmi
	 *            the output row format
	 */
	public void setOutputRowMeta(RowMetaInterface rmi) {
		outputRowMeta = rmi;
	}
}
