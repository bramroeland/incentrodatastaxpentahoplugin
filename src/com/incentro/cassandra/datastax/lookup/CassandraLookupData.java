/**
 * Incentro
 */
package com.incentro.cassandra.datastax.lookup;

import java.util.ArrayList;

import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.trans.step.BaseStepData;
import org.pentaho.di.trans.step.StepDataInterface;

/**
 * @author Chris Molanus
 */
public class CassandraLookupData extends BaseStepData implements StepDataInterface {

	public RowMetaInterface inputRowMeta;
	public RowMetaInterface outputRowMeta;
	public ArrayList<String> ps;

}
