/**
 * Incentro
 */
package com.incentro.cassandra.datastax.lookup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.pentaho.di.core.Const;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMeta;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStep;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.incentro.cassandra.datastax.CassandraConnection;
import com.incentro.cassandra.datastax.CassandraConnectionException;
import com.incentro.cassandra.datastax.Utils;

/**
 * @author Chris Molanus
 *
 */
public class CassandraLookup extends BaseStep implements StepInterface {

	private static Class<?> PKG = CassandraLookup.class;

	private CassandraLookupMeta meta;
	private CassandraLookupData data;

	private List<String> rowColumns;

	private List<Integer> columnMapping;

	private CassandraConnection connection;

	private String nodes;

	private String port;

	private String username;

	private String password;

	private String keyspace;

	private Boolean withSSL;

	private String trustStoreFilePath;

	private String trustStorePass;

	private String compression;

	public CassandraLookup(StepMeta stepMeta, StepDataInterface stepDataInterface, int copyNr, TransMeta transMeta,
			Trans trans) {
		super(stepMeta, stepDataInterface, copyNr, transMeta, trans);
	}

	public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException {

		Object[] r = getRow(); // Get row from input rowset & set row busy!
		if (r == null) { // no more input to be expected...

			setOutputDone(); // signal end to receiver(s)
			return false;
		}

		if (first) {
			first = false;
			meta = (CassandraLookupMeta) smi;
			data = (CassandraLookupData) sdi;

			data.inputRowMeta = getInputRowMeta().clone();
			data.outputRowMeta = getInputRowMeta().clone();
			meta.getFields(data.outputRowMeta, getStepname(), null, null, this, repository, metaStore);
			rowColumns = Arrays.asList(data.inputRowMeta.getFieldNames());
			columnMapping = new ArrayList<Integer>();
			for (String compareName : meta.keyCompareFieldName) {
				if (!Const.isEmpty(environmentSubstitute(compareName))) {
					columnMapping.add(rowColumns.indexOf(environmentSubstitute(compareName)));
				}
			}
			createQuery();

		}

		try {
			Object[] outputRow = lookupValues(data.inputRowMeta, r); // add new values to the row in rowset[0].
			putRow(data.outputRowMeta, outputRow); // copy row to output rowset(s);
		} catch (KettleException e) {
			logError(BaseMessages.getString(PKG, "DimensionLookup.Log.StepCanNotContinueForErrors", e.getMessage()));
			logError(Const.getStackTracker(e));
			setErrors(1);
			stopAll();
			setOutputDone(); // signal end to receiver(s)
			return false;
		}

		return true;

	}

	private Object[] lookupValues(RowMetaInterface inputRowMeta, Object[] r) {
		Object[] outputRow = new Object[data.outputRowMeta.size()];

		// Copy the results to the output row...
		// First copy the input row values to the output..
		//
		for (int i = 0; i < inputRowMeta.size(); i++) {
			outputRow[i] = r[i];
		}

		int outputIndex = inputRowMeta.size();

		StringBuilder sql = new StringBuilder();
		int columnMappingIndex = 0;
		for (String sqlPart : data.ps) {
			sql.append(sqlPart);
			if (columnMappingIndex < columnMapping.size()) { // Loop will end with last part of sqlParts
				sql.append(r[columnMapping.get(columnMappingIndex)].toString());
				columnMappingIndex++;
			}
		}
		try {
			Row row = null;
			try {
				ResultSet results = getSession().execute(sql.toString());
				row = results.one(); // Only get the first row
			}
			catch (NullPointerException e) {
				throw new KettleStepException("Failed to execute query, connection closed?");
			}

			if (row != null) {
				for (String name : meta.returnValueName) {
					// Assume that the Query returns the columns in the same order
					// that they are filled in on the interface, see createQuery()
					switch (data.outputRowMeta.getValueMeta(outputIndex).getType()) {
					case ValueMeta.TYPE_STRING:
						outputRow[outputIndex] = row.getString(environmentSubstitute(name));
						break;
					case ValueMeta.TYPE_BIGNUMBER:
						outputRow[outputIndex] = new java.math.BigDecimal(row.getLong(environmentSubstitute(name)));
						break;
					case ValueMeta.TYPE_NUMBER:
						outputRow[outputIndex] = new Double(row.getDouble(environmentSubstitute(name)));
						break;
					case ValueMeta.TYPE_INTEGER:
						outputRow[outputIndex] = new Integer(row.getInt(environmentSubstitute(name)));
						break;
					case ValueMeta.TYPE_DATE:
						outputRow[outputIndex] = row.getDate(environmentSubstitute(name));
						break;
					case ValueMeta.TYPE_SERIALIZABLE:
						outputRow[outputIndex] = row.getBytesUnsafe(environmentSubstitute(name));
						break;
					case ValueMeta.TYPE_BINARY:
						outputRow[outputIndex] = row.getBytes(environmentSubstitute(name));
						break;
					case ValueMeta.TYPE_TIMESTAMP:
						outputRow[outputIndex] = new Long(row.getLong(environmentSubstitute(name)));
						break;
					case ValueMeta.TYPE_INET:
						outputRow[outputIndex] = row.getString(environmentSubstitute(name));
						break;
					}
					outputIndex++;
				}
			}
		} catch (KettleStepException e) {
			logError("Could not retieve data: " + e.getMessage());
		}

		return outputRow;
	}

	public boolean init(StepMetaInterface smi, StepDataInterface sdi) {
		// Casting to step-specific implementation classes is safe
		meta = (CassandraLookupMeta) smi;
		data = (CassandraLookupData) sdi;

		nodes = environmentSubstitute(meta.getCassadraHost());
		port = environmentSubstitute(meta.getCassandraPort());
		username = environmentSubstitute(meta.getUsername());
		password = environmentSubstitute(meta.getPassword());
		keyspace = environmentSubstitute(meta.getKeyspace());
		withSSL = meta.getWithSSL();
		trustStoreFilePath = environmentSubstitute(meta.getTrustStoreFilePath());
		trustStorePass = environmentSubstitute(meta.getTrustStorePass());
		compression = meta.getCompression();

		try {
			this.connection = Utils.connect(nodes, port, username, password, keyspace, withSSL,
					trustStoreFilePath, trustStorePass, compression);
		} catch (CassandraConnectionException e) {
			logError("Could initialize step: " + e.getMessage());
			return false;
		}

		return super.init(meta, data);
	}

	@Override
	public void dispose(StepMetaInterface smi, StepDataInterface sdi) {
		if (connection != null)
			connection.release();
		super.dispose(meta, data);
	}

	public Session getSession() {
		return this.connection != null ? this.connection.getSession() : null;
	}

	public void createQuery() throws KettleStepException {
		Cluster cluster = getSession().getCluster();
		List<ColumnMetadata> column = cluster.getMetadata().getKeyspace(environmentSubstitute(meta.getKeyspace()))
				.getTable(environmentSubstitute(meta.getColumnFamily())).getColumns();
		HashMap<String, Boolean> fieldTypeIsTextMapping = new HashMap<String, Boolean>();
		rowColumns = Arrays.asList(data.inputRowMeta.getFieldNames());
		Iterator<ColumnMetadata> iter = column.iterator();
		while (iter.hasNext()) {
			ColumnMetadata columnRow = iter.next();

			fieldTypeIsTextMapping.put(columnRow.getName(), columnRow.getType().getName().name().equals("TEXT"));
			if (rowColumns.contains(columnRow.getName())) {
				if (!fieldTypeIsTextMapping.get(columnRow.getName())) {
					if (columnRow.getType().getName().name().equals("INT") && meta.getReturnValueDefaultType()
							.get(rowColumns.indexOf(columnRow.getName())) != ValueMeta.TYPE_INTEGER) {
						throw new KettleStepException("Column " + columnRow.getName()
								+ " type ( INT ) in Cassandra does not match field type in Pentaho ");
					} else
						if (columnRow.getType().getName().name().equals("BIGINT") && (meta.getReturnValueDefaultType()
								.get(rowColumns.indexOf(columnRow.getName())) != ValueMeta.TYPE_BIGNUMBER
								&& meta.getReturnValueDefaultType()
										.get(rowColumns.indexOf(columnRow.getName())) != ValueMeta.TYPE_INTEGER)) {
						throw new KettleStepException("Column " + columnRow.getName()
								+ " type ( BIGINT ) in Cassandra does not match field type in Pentaho ");
					}
				}

			}
		}

		rowColumns = Arrays.asList(data.inputRowMeta.getFieldNames());
		ArrayList<Integer> columnMapping2 = new ArrayList<Integer>();
		for (String compareName : meta.keyCompareFieldName) {
			if (!Const.isEmpty(environmentSubstitute(compareName))) {
				columnMapping2.add(rowColumns.indexOf(environmentSubstitute(compareName)));
			} else {
				columnMapping2.add(-1);
			}
		}

		// Split up into parts because we will use a string builder later.
		// Better performance since String builder is faster that String.replaceFirst
		ArrayList<String> sqlParts = new ArrayList<String>();
		StringBuilder sqlPart = new StringBuilder();
		sqlPart.append("SELECT ");
		boolean first = true;
		for (String columnName : meta.returnValueName) {
			if (!first) {
				sqlPart.append(',');
			} else {
				first = false;
			}
			sqlPart.append(environmentSubstitute(columnName));
		}
		sqlPart.append(" FROM ");
		sqlPart.append(environmentSubstitute(meta.columnFamily));
		if (meta.keyName.size() > 0) {
			sqlPart.append(" WHERE ");
			boolean firstField = true;
			for (int i = 0; i < meta.keyName.size(); i++) {
				if (!firstField) {
					sqlPart.append(" AND ");
				} else {
					firstField = false;
				}
				sqlPart.append(environmentSubstitute(meta.keyName.get(i)));
				sqlPart.append(' ').append(meta.keyCondition.get(i)).append(' ');

				if (Const.isEmpty(environmentSubstitute(meta.keyCompareFieldName.get(i)))) {
					sqlPart.append('\'');
					sqlPart.append(meta.keyCompareValue.get(i));
					sqlPart.append('\'');
				} else {
					if (fieldTypeIsTextMapping.get(environmentSubstitute(meta.keyName.get(i)))) {
						sqlPart.append('\'');
					}
					sqlParts.add(sqlPart.toString());
					sqlPart = new StringBuilder();
					if (fieldTypeIsTextMapping.get(environmentSubstitute(meta.keyName.get(i)))) {
						sqlPart.append('\'');
					}
				}

			}
		}
		sqlPart.append(';');
		sqlParts.add(sqlPart.toString());
		logBasic("Before: " + sqlParts.toString());
		data.ps = sqlParts;
	}

}
